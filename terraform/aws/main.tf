resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCgALvh8ECbpFLoUo0XkhoSqTOWFGLmOEHqRWFeKnA3T6f7dPqP9e+XHZdBR2bUd7NQPeZaqtOBIE8khgpFJdjNAxXqGXOR+vi2K1TOeo7RSS19ZDwY4/udwzI2MVyxMhFI80hRbwuGSxEy10W6Fy8lCUPg3r+PwXcqvkH+mR6yOAeqlLnwTinN+ju2hcurRCawZ7l9jPuNedn60MCz3wo1xEh9F9+nDLeFLve2LhGhlHM6WF1LtKE7CPW2V+c3eGnM+k/bfBtiuvfhT4Lkt2X6dK51s2Attob1WtOmQnP08BXN3fG+b+d6AVW1hM3+4xf9+vfh4xiXJYw6muDPQ1llCxOlmF2VTo+q4MMRQoLzcbsgzbEiAmdtfDpu3Q85Iqj//SBmfr3FNbt8BAEZN/oHEbRq3OfIBxxfYmQx5hq0n3Rcf8lSvoYIEzMBiDhshGsl29mM4PFrhXymgLKIPTf0b9dh2eRHmN9ui8mBdayK2algOmQ0+aljhKYrVcLLWrM= marin@marin"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_default_security_group" "default" {
  vpc_id =aws_default_vpc.default.id

  ingress {
    protocol  = "tcp"
    from_port = 80
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = "tcp"
    from_port = 22
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

provider "aws" {
  region = "us-west-1"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.deployer.key_name}"

  tags = {
    Name = "HelloWorld"
  }
}