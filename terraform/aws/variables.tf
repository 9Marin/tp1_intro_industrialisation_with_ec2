variable "region" {
  description = "The aws region the resources will be build"
  default= "eu-west-2c"
  type        = string
}
variable "aws_private_key_ssh_path"{
  default="~/.ssh/id_rsa"
}

variable "aws_public_key_ssh_path"{
  default="~/.ssh/id_rsa.pub"
}

variable "ami_id" {
  description = "id of an ami by default it's ubuntu 16.04"
  type        = string
  default     = "ami-0aaa5410833273cfe"
}

variable "instance_type" {
  description = "aws ec2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "tag_name" {
  description = "Aws tag name permit to search an instance by tag"
  type        = string
}